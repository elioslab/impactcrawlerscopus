﻿# README

L’applicazione ImpactCrawler ha lo scopo di permettere l’estrazione di informazioni di impatto scientifico (h-index, numero di pubblicazioni, citazioni) dal database Scopus e di calcolare indicazioni statistiche (mediane, distribuzioni) per una popolazione definita (SSD, Università, Dipartimento) della comunità universitaria italiana, così come descritta sul database Cineca.

## Descrizione dell’algoritmo

L'algoritmo implementato in ImpactCrawler per la misura delle informazioni di impatto scientifico è composto dai seguenti passi:

1. L’algoritmo interroga  il database [Cineca “Cerca Università”][1], che contiene l’elenco dei docenti delle Università italiane. La ricerca è impostata inserendo come SSD “ING-INF/01”, ma è possibile definire anche altri tipi di ricerca. Purtroppo il sistema Cineca non è pensato per una interfaccia “machine readable”, così il software compone una richiesta POST analizzando il contenuto della pagina e estrae le informazioni sull’elenco dei docenti effettuando il parser della pagina HTML restituita dal sito. Il log delle informazioni ottenute è esportato nel file “log_members_cineca.txt”. E’ possibile estrarre analoghe informazioni per altri SSD (basta utilizzare una diversa stringa per l’interrogazione di Cineca all'interno del software) o per altri aggregati (ad esempio, Università o Dipartimenti) utilizzando diverse chiavi di ricerca.

2. Utilizzando l’elenco dei docenti ottenuto al punto 1), l’algoritmo interroga le API messe a disposizione da [Elsevier][2]. Per l’utilizzo di queste API è necessario ottenere una API key e collegarsi da un IP di una istituzione con un abbonamento Scopus. Per questo il software realizzato, per poter essere usato da altri, necessità l’inserimento di una key valida e di una postazione con IP valido. L’interrogazione delle API ha lo scopo di individuare lo "Scopus ID" univoco di ogni docente. Per questo viene effettuata una ricerca utilizzando cognome e nome del docente e risolvendo le omonimie utilizzando l’istituzione di provenienza e le aree di interesse scientifico. Il log delle informazioni ottenute è esportato nel file “log_ids_scopus.txt”. Queste informazioni sono affette da rumore, poiché non è certa l’attribuzione del giusto Scopus ID per ogni docente estratto da Cineca. Per eliminare le incertezze, l’algoritmo importa dati forniti dall’Associazione GE contenenti tutti gli Scopus ID dei membri. Queste informazioni sono contenute nel file “ge-scopus-id.csv”. Se l’ID estratto dalle API di Scopus è identico a quello trovato nel file GE, allora l’algoritmo associa il docente a tale ID. Altrimenti vi possono essere diversi casi dubbi che sono indicati nel file “warning.txt”. I casi dubbi sono così trattati: se l’ID estratto da Scopus è diverso da quello trovato sul file GE, viene utilizzato quello del file GE; se non esiste un ID nel file GE (non tutti gli strutturati ING-INF/01 sono anche membri del GE) allora viene usato l’ID individuato da Scopus. 

3. Utilizzando l’elenco degli ScopusID ottenuto al punto 2), l’algoritmo interroga nuovamente le API Elsevier per ottenere per ogni docente l’elenco delle sue pubblicazioni con le seguenti informazioni: identificativo scopus, titolo, numero di citazioni, anno di pubblicazione e tipo di pubblicazione (rivista/conferenza/altro). Il log delle informazioni ottenute è esportato nel file “log_papers_scopus.txt”

4. Per ogni docente estratto dal database cineca viene controllata l'appartenenza all'associazione GE utilizzando le informazioni messe a disposizione dall'associazione e contenute nel file "ge-members.txt", nel caso l'appartenenza si appurata, vengono individuate le seguenti informazioni: area e unità di ricerca.

5. L’algoritmo elabora le informazioni ottenute al punto 3) per costruire alcune metriche (h-index, numero di documenti totale, numero di pubblicazioni su rivista, numero di pubblicazioni su conferenza, h-index) per ogni docente e per diversi periodi di tempo: assoluto, ultimi 10 anni, ultimi 15 anni. Il log delle informazioni ottenute è esportato nel file “log_metrics_evaluation.txt”.

6. L'algoritmo elabora le informazioni ottenute al punto 5) per determinare alcuni dati statistici aggregati (mediane, distribuzioni e percentili), su alcune sotto-popolazioni (tutti i docenti, solo i ricercatori, solo gli associati, solo gli ordinari), per diversi periodi temporali (sempre, ultimi 10 anni, ultimi 15 anni) e per diverse aree di ricerca GE. I dati ottenuti sono esportati nel file "log_medians_evaluation.txt", "log_distributions_evaluation.txt" e "log_percentile_evaluation.txt"

7. Infine l’algoritmo esporta i dati in vari formati di tipo "machine readable": 
- viene generato un file JSON con tutte le informazioni raccolte e che può essere importato in altri strumenti per successive elaborazioni (vedi file “ssd.json”), ad esempio importando la stringa contenuta nel file nello strumento online [jsonviewer][3]; 
- viene generata una serie di files CSV (ssd_all.csv, ssd_associati.csv, ssd_ricercatori.csv, ssd_ordinari.csv) che contengono i dati prodotti al punto 3), in modo che possano essere analizzati per ulteriori elaborazioni statistiche;
- viene generato un file CSV (medians.csv) che contiene i valori delle mediane per i vari indicatori di impatto
- viene generata una serie di files CSV (distribution_citations.csv, distribution_conferences.csv, distribution_hindex.csv, distribution_journals, distribution_papers.csv) che riportano i valori delle varie distribuzioni calcolate
- anche per i percentili vengono generati gli stessi file delle distribuzioni
- per ogni distribuzione e per ogni percentile viene prodotto un grafico in formato jpeg.

Per ulteriori informazioni contattare riccardo.berta[at]unige.it

[1]:	http://cercauniversita.cineca.it/php5/docenti/cerca.php
[2]:	http://dev.elsevier.com/index.html
[3]:	http://jsonviewer.stack.hu/

