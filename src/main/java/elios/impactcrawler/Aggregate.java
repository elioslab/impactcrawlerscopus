package elios.impactcrawler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;

public class Aggregate 
{
    private String name;
    private String status = "empty";
    private List<Member> members;
    private List<Metric> medians;
    private List<Distribution> frequencies;
    private List<Distribution> percentiles;

    public Aggregate() { }

    public void setName(String name) { this.name = name; }
    public String getName() { return name; }
    public void setMembers(List<Member> members) { this.members = members; }
    public List<Member> getMembers() { return members; }
    public List<Metric> getMedians() { return medians; }
    public void setMedians(List<Metric> medians) { this.medians = medians; }
    public List<Distribution> getFrequencies() { return frequencies; }
    public void setFrequencies(List<Distribution> frequencies) { this.frequencies = frequencies; }
    public List<Distribution> getPercentiles() { return percentiles; }
    public void setPercentiles(List<Distribution> percentiles) { this.percentiles = percentiles; } 
    public void setStatus(String status) { this.status = status; }
    public String getStatus() { return status; }

    static public Aggregate load() {
        InputStream file = null;
        try {
           if (Files.exists(Paths.get("output/aggregate.ser"))) {
                file = new FileInputStream("output/aggregate.ser");
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput input = new ObjectInputStream (buffer);

                Aggregate aggregate = new Aggregate();
                aggregate.setName((String)input.readObject());
                aggregate.setStatus((String)input.readObject());
                aggregate.setMembers((List<Member>)input.readObject());
                aggregate.setFrequencies((List<Distribution>)input.readObject());
                aggregate.setPercentiles((List<Distribution>)input.readObject());
                aggregate.setMedians((List<Metric>)input.readObject());
                
                return aggregate;
           }
        } 
        catch (Exception ex) {
            return null;
        } 
        finally {
            try {
                if (Files.exists(Paths.get("output/aggregate.ser"))) {
                    file.close();
                }
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Aggregate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    static public void save(Aggregate aggregate) {
        try {
            OutputStream file = new FileOutputStream("output/aggregate.ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);
            output.writeObject(aggregate.getName());
            output.writeObject(aggregate.getStatus());
            output.writeObject(aggregate.getMembers());
            output.writeObject(aggregate.getFrequencies());
            output.writeObject(aggregate.getPercentiles());
            output.writeObject(aggregate.getMedians());
            output.close();
        } 
        catch (IOException ex) {
            java.util.logging.Logger.getLogger(Aggregate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void readMembersFile(String memberFileName) {
        ClassLoader classLoader = getClass().getClassLoader();
            
        try {
            BufferedReader br = new BufferedReader(new FileReader(classLoader.getResource(memberFileName).getFile()));
            String line = br.readLine();

            while (line != null) {
                String[] info = line.split(";");
                
                String name = info[0];
                String surname = info[1]; 
                String role = info[2];
                String ssd = info[3];
                String scopusid = info[4]; 
                
                members.add(new Member(name, surname, role, ssd, scopusid));
                
                line = br.readLine();
            }
            br.close();
        } 
        catch (IOException e)  {
            e.printStackTrace();
	}
    }
}
