package elios.impactcrawler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Evaluator 
{
    private final Aggregate aggregate;

    public Evaluator(Aggregate aggregate) {
        this.aggregate = aggregate;
    }
    
    private Map<String, Integer> fillDistribution(int[] array, int step) {
        Map<String, Integer> distribution = new LinkedHashMap<>();
        
        int start = 0;
        int stop = step;
        for(int i=0; i<array.length; i++)
        {
            String temp = "";
            
            if(i == array.length-1)
            {
                temp += "[>" + start + "]";
            }
            else
            {
                if(step == 1)
                    temp += "[" + start + "]";
                else
                    temp += "[" + start + "-" + stop + "]";
            }
            
            distribution.put(temp, array[i]);
            
            start += step;
            stop += step;
        }
        
        return distribution;
    }
           
    public Distribution frequency(String name, Integer index, String role)
    {
        List<Member> total_members = aggregate.getMembers();
        List<Member> members = new ArrayList<>();
        
        Integer start_year = total_members.get(0).getMetrics().get(index).getStartYear();
        Integer stop_year = total_members.get(0).getMetrics().get(index).getStopYear();
            
        for(int i=0; i<total_members.size(); i++) {
            Member member_temp = total_members.get(i);
            if(role == null || member_temp.getRole().contains(role))
                members.add(member_temp);
        }
        
        int[] hindexes = new int[members.size()];
        int[] documents = new int[members.size()];
        int[] citations = new int[members.size()];
        int[] journals = new int[members.size()];
        int[] conferences = new int[members.size()];
        
        for(int i=0; i<members.size(); i++) {
            Member member_temp = members.get(i);
            
            documents[i] = member_temp.getMetrics().get(index).getDocumentCount();
            citations[i] = member_temp.getMetrics().get(index).getCitationCount();
            hindexes[i] = member_temp.getMetrics().get(index).getHindex();
            journals[i] = member_temp.getMetrics().get(index).getJournalDocumentCount();
            conferences[i] = member_temp.getMetrics().get(index).getConferenceDocumentCount();
        }
        
        Statistics hindexes_stat = new Statistics(hindexes);
        Statistics documents_stat = new Statistics(documents);
        Statistics citations_stat = new Statistics(citations);
        Statistics journals_stat = new Statistics(journals);
        Statistics conferences_stat = new Statistics(conferences);
        
        int hindexes_step = 1;
        int documents_step = 5;
        int citations_step = 20;
        int journals_step =  5;
        int conferences_step = 5;
        
        int[] hindexes_frequency = hindexes_stat.getFrequencies(hindexes_step);
        int[] documents_frequency = documents_stat.getFrequencies(documents_step);
        int[] citations_frequency = citations_stat.getFrequencies(citations_step);
        int[] journals_frequency = journals_stat.getFrequencies(journals_step);
        int[] conferences_frequency = conferences_stat.getFrequencies(conferences_step);
        
        Map<String, Integer> hindexes_distribution = fillDistribution(hindexes_frequency, hindexes_step);
        Map<String, Integer> documents_distribution = fillDistribution(documents_frequency, documents_step);
        Map<String, Integer> citations_distribution = fillDistribution(citations_frequency, citations_step);
        Map<String, Integer> journals_distribution = fillDistribution(journals_frequency, journals_step);
        Map<String, Integer> conferences_distribution = fillDistribution(conferences_frequency, conferences_step);
            
        Distribution distribution = new Distribution(name, start_year, stop_year, index, hindexes_distribution, documents_distribution, journals_distribution, conferences_distribution, citations_distribution);
        
        return distribution;
    }
    
    public Distribution percentile(String name, Integer index, String role) {
        List<Member> total_members = aggregate.getMembers();
        List<Member> members = new ArrayList<>();
        
        Integer start_year = total_members.get(0).getMetrics().get(index).getStartYear();
        Integer stop_year = total_members.get(0).getMetrics().get(index).getStopYear();
            
        for(int i=0; i<total_members.size(); i++) {
            Member member_temp = total_members.get(i);
            if(role == null || member_temp.getRole().contains(role))
                members.add(member_temp);
        }
        
        int[] hindexes = new int[members.size()];
        int[] documents = new int[members.size()];
        int[] citations = new int[members.size()];
        int[] journals = new int[members.size()];
        int[] conferences = new int[members.size()];
        
        for(int i=0; i<members.size(); i++) {
            Member member_temp = members.get(i);
            
            documents[i] = member_temp.getMetrics().get(index).getDocumentCount();
            citations[i] = member_temp.getMetrics().get(index).getCitationCount();
            hindexes[i] = member_temp.getMetrics().get(index).getHindex();
            journals[i] = member_temp.getMetrics().get(index).getJournalDocumentCount();
            conferences[i] = member_temp.getMetrics().get(index).getConferenceDocumentCount();
        }
        
        Statistics hindexes_stat = new Statistics(hindexes);
        Statistics documents_stat = new Statistics(documents);
        Statistics citations_stat = new Statistics(citations);
        Statistics journals_stat = new Statistics(journals);
        Statistics conferences_stat = new Statistics(conferences);
        
        int hindexes_step = 1;
        int documents_step = 5;
        int citations_step = 20;
        int journals_step =  5;
        int conferences_step = 5;
        
        int[] hindexes_percentile = hindexes_stat.getPercentile(hindexes_step);
        int[] documents_percentile = documents_stat.getPercentile(documents_step);
        int[] citations_percentile = citations_stat.getPercentile(citations_step);
        int[] journals_percentile = journals_stat.getPercentile(journals_step);
        int[] conferences_percentile = conferences_stat.getPercentile(conferences_step);
        
        Map<String, Integer> hindexes_distribution = fillDistribution(hindexes_percentile, hindexes_step);
        Map<String, Integer> documents_distribution = fillDistribution(documents_percentile, documents_step);
        Map<String, Integer> citations_distribution = fillDistribution(citations_percentile, citations_step);
        Map<String, Integer> journals_distribution = fillDistribution(journals_percentile, journals_step);
        Map<String, Integer> conferences_distribution = fillDistribution(conferences_percentile, conferences_step);
            
        Distribution percentile = new Distribution(name, start_year, stop_year, index, hindexes_distribution, documents_distribution, journals_distribution, conferences_distribution, citations_distribution);
        
        return percentile;
    }
    
    public Metric median(String name, Integer index, String role) {   
        List<Member> members = aggregate.getMembers();
        
        List<Integer> hindexes = new ArrayList<>();
        List<Integer> citations = new ArrayList<>();
        List<Integer> totalPapers = new ArrayList<>();
        List<Integer> journalPapers = new ArrayList<>();
        List<Integer> conferencePapers = new ArrayList<>();
        
        Integer start_year = 0;
        Integer stop_year = 0;
        
        for(int i=0; i<members.size(); i++) {
            Member member = members.get(i);
            
            start_year = member.getMetrics().get(index).getStartYear();
            stop_year = member.getMetrics().get(index).getStopYear();
            
            if(member.getRole().contains(role)) {
                hindexes.add(member.getMetrics().get(index).getHindex());
                citations.add(member.getMetrics().get(index).getCitationCount());
                totalPapers.add(member.getMetrics().get(index).getDocumentCount());
                journalPapers.add(member.getMetrics().get(index).getJournalDocumentCount());
                conferencePapers.add(member.getMetrics().get(index).getConferenceDocumentCount());
            }
        }
        
        Integer median_hindex = this.calculateMedianValue(hindexes);
        Integer median_citations = this.calculateMedianValue(citations);
        Integer median_totalPapers = this.calculateMedianValue(totalPapers);
        Integer median_journalPapers = this.calculateMedianValue(journalPapers);
        Integer median_conferencePapers = this.calculateMedianValue(conferencePapers);
        
        Metric metric = new Metric(name, median_hindex, median_totalPapers, median_journalPapers, median_conferencePapers, median_citations, start_year, stop_year);
        
        return metric;
    }
    
    public Metric metric(Member member, String name, Integer start_year, Integer stop_year) {
        Integer totalPaperCount = 0;
        Integer journalPaperCount = 0;
        Integer conferencePaperCount = 0;
        Integer otherPaperCount = 0;
        Integer hindex = 0;
        
        Integer citationCount = 0;
        Integer[] citations = new Integer[1000];
        
        for(int i=0; i<citations.length; i++)
            citations[i] = 0;
        
        List<Paper> papers = member.getPapers();
        
        if(papers != null) {
            for(int i=0; i<papers.size(); i++) {
                Paper paper = papers.get(i);
                if( (paper.getYear() != null) && (!paper.getYear().equals("unknown")) ) {
                    Integer year = 1900;
                    try {
                        year = Integer.parseInt(paper.getYear().substring(paper.getYear().length() - 4));
                    }
                    catch(Exception e) { 
                        Logger.warning("Paper (" + paper.getTitle() + ") of " + member.getSurname() + " with invalid date: " +  paper.getYear());
                    }
                    if( (year >= start_year) && (year <= stop_year) ) {
                        totalPaperCount++;

                        if(paper.getType() != null) {
                            if(paper.getType().equals("Journal"))
                                journalPaperCount++;
                            else if(paper.getType().equals("Conference Proceeding"))
                                conferencePaperCount++;
                            else
                                otherPaperCount++;
                        }

                        if(paper.getCitations() != null) {
                            citationCount += Integer.parseInt(paper.getCitations());
                            citations[i] = Integer.parseInt(paper.getCitations());
                        }
                    }
                }
            }

            Arrays.sort(citations, Collections.reverseOrder());

            for(int i=citations.length-2; i>0; i--)
                citations[i+1] = citations[i];

            for(int i=1; i<citations.length; i++) {
                if(citations[i] >= i)
                    hindex = i;
            }   
        }
        
        Metric metric = new Metric(name, hindex, totalPaperCount, journalPaperCount, conferencePaperCount, citationCount, start_year, stop_year);
        return metric;
    }
    
    private Integer calculateMedianValue(List<Integer> values) {
        Integer result;
        
        Collections.sort(values);
        
        Integer middle = values.size()/2;
 
        if (values.size() % 2 == 1) {
            result = values.get(middle);
        } 
        else {
           Double temp = (values.get(middle-1) + values.get(middle)) / 2.0;
           result = temp.intValue();
        }
        
        return result;
    }
}
