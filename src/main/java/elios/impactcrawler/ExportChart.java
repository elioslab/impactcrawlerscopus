package elios.impactcrawler;

import java.io.*;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.ChartUtilities;

public class ExportChart 
{
   public void generate(Map<String, Integer> data, String title, String xLabel, String yLabel)throws Exception 
   {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
      
        Object[] keys = data.keySet().toArray();
        Object[] values = data.values().toArray();
      
        for(int i=0; i<keys.length; i++)
            dataset.addValue( (Integer)values[i] , " " , (String)keys[i]);
      
        JFreeChart chart = ChartFactory.createBarChart(
            title, 
            xLabel, 
            yLabel, 
            dataset,
            PlotOrientation.VERTICAL, 
            false, 
            true, 
            false);
      
        //Font font = new Font("Dialog", Font.PLAIN, 12);
        //CategoryPlot p = chart.getCategoryPlot(); 
        //CategoryAxis axisCategory = p.getDomainAxis();
        //ValueAxis axisValue = p.getRangeAxis();
        //axisValue.setAutoTickUnitSelection(true);
        //CategoryAxis axisCategory = p.getDomainAxis();
        //axisValue.setTickLabelFont(font);
        //axisCategory.setTickLabelFont(font);
         
        int width = 1500;
        int height = 700;
      
        File file = new File("output/images/" + title + ".jpeg");
        file.delete();
      
        ChartUtilities.saveChartAsJPEG(file, chart, width, height);
   }
}

