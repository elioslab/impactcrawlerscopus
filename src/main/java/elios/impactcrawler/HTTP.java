package elios.impactcrawler;

import java.io.InputStream;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;

public class HTTP 
{
    public String sendPost(String url, List<NameValuePair> body) throws Exception 
    {
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost(url);
        httppost.setEntity(new UrlEncodedFormEntity(body, "UTF-8"));

        HttpResponse response = httpclient.execute(httppost);
        HttpEntity entity = response.getEntity();
        InputStream instream = entity.getContent();
        String myString = IOUtils.toString(instream, "UTF-8");    
        return myString;
    }
    
    public String sendGet(String url_param) throws Exception 
    {
        HttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(url_param);
        HttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        InputStream instream = entity.getContent();
        String myString = IOUtils.toString(instream, "UTF-8");    
        return myString;
    }
}
