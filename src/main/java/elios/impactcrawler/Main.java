package elios.impactcrawler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        String MembersFileName = "members.csv";

        Logger.setVerbose(true);

        File dir = new File("output");
        dir.mkdir();

        dir = new File("output/images");
        dir.mkdir();

        dir = new File("output/frequencies");
        dir.mkdir();

        dir = new File("output/percentiles");
        dir.mkdir();

        dir = new File("output/logs");
        dir.mkdir();

        getMembers(MembersFileName);
        getPapers();
        evaluateMetrics();
        exportJSON_CSV_Charts();
    }

    public static void getMembers(String membersFileName) throws Exception {
        Logger.setFileName(null);

        Logger.write(" ");
        Logger.write("----------------------------------");
        Logger.write("Extracting member of the aggregate");
        Logger.write("----------------------------------");

        Aggregate aggregate = Aggregate.load();
        if (aggregate != null) {
            Logger.write("Already available");
        } else {
            Logger.write("Reading information...");
            Logger.setFileName("output/logs/log_members.txt");
            aggregate = new Aggregate();
            aggregate.setName("Diten");
            aggregate.readMembersFile(membersFileName);
            aggregate.setStatus("AfterReadingMemebers");
            Aggregate.save(aggregate);
        }
    }
    
    public static void getPapers() throws Exception {
        Scopus scopus = new Scopus();

        Logger.setFileName(null);

        Logger.write(" ");
        Logger.write("--------------------------------");
        Logger.write("Searching for papers from Scopus");
        Logger.write("--------------------------------");

        Aggregate aggregate = Aggregate.load();
        if (!aggregate.getStatus().equals("AfterReadingMemebers")) {
            Logger.write("Already available");
        } else {
            Logger.write("Crawling information...");

            Logger.setFileName("output/logs/log_papers_scopus.txt");

            List<Member> list = aggregate.getMembers();
            for (int i = 0; i < list.size(); i++) {
                Member member = list.get(i);

                Logger.write("[" + i + "] " + member.toString());

                if (member.getScopusid() != null) {
                    List<Paper> papers = scopus.getPapers(member.getScopusid());
                    member.setPapers(papers);
                }
            }

            aggregate.setStatus("AfterScopusPapers");
            aggregate.save(aggregate);
        }
    }
    
    public static void evaluateMetrics() throws Exception {
        Logger.setFileName(null);

        Logger.write(" ");
        Logger.write("------------------");
        Logger.write("Evaluating  metric");
        Logger.write("------------------");

        Aggregate aggregate = Aggregate.load();
        if (!aggregate.getStatus().equals("AfterScopusPapers")) {
            Logger.write("Already available");
        } else {
            Logger.write("Calculating information...");

            Logger.setFileName("output/logs/log_metrics_evaluation.txt");

            List<Member> list = aggregate.getMembers();
            for (int i = 0; i < list.size(); i++) {
                List<Metric> metrics = new ArrayList<>();
                Member member = list.get(i);

                Evaluator evaluator = new Evaluator(aggregate);

                Metric evaluated_absolute = evaluator.metric(member, "absolute", 1900, 2016);
                Metric evaluated_2012 = evaluator.metric(member, "2012", 2008, 2012);
                Metric evaluated_2013 = evaluator.metric(member, "2013", 2009, 2013);
                Metric evaluated_2014 = evaluator.metric(member, "2014", 2010, 2014);

                metrics.add(evaluated_absolute);
                metrics.add(evaluated_2012);
                metrics.add(evaluated_2013);
                metrics.add(evaluated_2014);

                member.setMetrics(metrics);

                Logger.write("[" + i + "] " + member.toString());
                Logger.write(" - absolute " + evaluated_absolute.toString());
                Logger.write(" - 2012 " + evaluated_2012.toString());
                Logger.write(" - 2013 " + evaluated_2013.toString());
                Logger.write(" - 2014 " + evaluated_2014.toString());
                Logger.write(" ");
            }

            aggregate.setStatus("AfterMetrics");
            Aggregate.save(aggregate);
        }
    }

    public static void exportJSON_CSV_Charts() throws IOException, Exception 
    {
        Logger.setFileName(null);

        Logger.write(" ");
        Logger.write("---------------------------------");
        Logger.write("Exporting to JSON, CSV and Charts");
        Logger.write("---------------------------------");

        Aggregate aggregate = Aggregate.load();
        if (aggregate.getStatus().equals("AfterMetrics")) {
            Logger.write(" - to JSON...");
            Utils.toJSONFile(aggregate);

            Logger.write(" - to CSV...");
            Utils.toCSVFile(aggregate);
            
            aggregate.setStatus("Complete");

            Aggregate.save(aggregate);
        } else {
            Logger.write("Information missing, impossible to export");
        }

        Logger.write(" ");
        Logger.write("--------");
        Logger.write("Complete");
        Logger.write("--------");
    }
}
