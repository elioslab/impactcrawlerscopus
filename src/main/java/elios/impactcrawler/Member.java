package elios.impactcrawler;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;

public class Member implements Serializable
{
    private final String name;
    private final String surname;
    private final String role;
    private final String ssd;
    private final String scopusid;
    
    private List<Metric> metrics = null;
    private List<Paper> papers = null;

    public Member(String name, String surname, String role, String ssd, String scopusid) 
    {
        this.name = name;
        this.surname = surname;
        this.role = role;
        this.ssd = ssd;
        this.scopusid = scopusid;
        
        Logger.write(" - " + surname +  " " + name + " (" + role + ", " + ssd + ", " + scopusid + ")");
    }

    public String getScopusid() { return scopusid; } 
    public String getName() { return name; }
    public String getSurname() { return surname; }
    public String getRole() { return role; }
    public String getSSD() { return ssd; }
    
    public void setMetrics(List<Metric> metrics) { this.metrics = metrics; }
    public List<Metric> getMetrics() { return metrics; }
    
    public void setPapers(List<Paper> papers) { this.papers = papers; }
    public List<Paper> getPapers() { return papers; }
    
    @Override
    public String toString() 
    {
        String description = "Member{";
                
        Field[] fields = Member.class.getDeclaredFields();
        for (Field field : fields) 
        {
            if(field.getGenericType() == String.class)
            {
                try {
                    if(field.get(this) != null)
                        description += " " + field.getName() + ":" + (String)field.get(this) + ";";
                } catch (IllegalArgumentException ex) {
                    java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        description += "}";
        
        return description;
    }   
}
