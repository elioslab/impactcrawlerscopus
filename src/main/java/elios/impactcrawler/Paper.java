package elios.impactcrawler;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.logging.Level;

public class Paper implements Serializable
{
    private final String id;
    private final String title;
    private final String citations;
    private final String year;
    private final String type;

    public Paper(String id, String title, String year, String type, String citations) 
    { 
        this.id = id; 
        this.title = title;
        this.type = type;
        this.year = year;
        this.citations = citations;
    }
    
    public String getTitle() { return title; }
    public String getCitations() { return citations; }
    public String getYear() { return year; }
    public String getType() { return type; }
    
    @Override
    public String toString() 
    {
        String description = "Paper{";
                
        Field[] fields = Paper.class.getDeclaredFields();
        for (Field field : fields) 
        {
            try {
                if(field.get(this) != null)
                    description += " " + field.getName() + ":" + (String)field.get(this) + ";";
            } catch (IllegalArgumentException ex) {
                java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        description += "}";
        
        return description;
    }
}
