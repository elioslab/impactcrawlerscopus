package elios.impactcrawler;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Scopus 
{
    //private final String api_key = "05c0133a54a3c916f49fc731b32bfdb0";
    //private final String api_key = "b3a71de2bde04544495881ed9d2f9c5b";
    private final String api_key = "0627ae4f375bbba2f9ecd8b049ba35db";
    
    private final String url = "https://api.elsevier.com/content";
    
    public String getAuthorAbsoluteMetrics(String scopusid) throws Exception {
        String query = "/author/author_id/" + scopusid + "?apiKey=" + api_key + "&view=METRICS&httpAccept=application/json";
                     
        HTTP http = new HTTP();
        String result = http.sendGet(url + query);
        
        return result;
    }

    Integer getCitationCount(String metadata) throws JSONException {
        JSONObject temp = new JSONObject(metadata);
        Integer citations = -1;
        
        try { citations = temp.getJSONArray("author-retrieval-response").getJSONObject(0).getJSONObject("coredata").getInt("citation-count"); } catch(Exception ex) {}
        
        return citations;
    }

    Integer getHindex(String metadata) throws JSONException {
        Integer hindex = -1;

        JSONObject temp = new JSONObject(metadata);

        try { hindex = temp.getJSONArray("author-retrieval-response").getJSONObject(0).getInt("h-index"); } catch(Exception ex) {}

        return hindex;
    }

    Integer getDocumentCount(String metadata) throws JSONException {
        JSONObject temp = new JSONObject(metadata);
        Integer documents = -1;
        
        try { documents =temp.getJSONArray("author-retrieval-response").getJSONObject(0).getJSONObject("coredata").getInt("document-count"); } catch(Exception ex) {}
        
        return documents;
    }
    
    List<Paper> getPapers(String scopusid) throws Exception {
        List<Paper> papers = new ArrayList<>();
        
        String query = null;
        String result = null;
        HTTP http = null;
        
        JSONObject temp = null;
        while(temp == null) {
            try {
                query = "/search/scopus/?query=AU-ID(" + scopusid.replace("9-s2.0-", "") + ")&apiKey=" + api_key;
            
                http = new HTTP();
                result = http.sendGet(url + query);
        
                temp = new JSONObject(result).getJSONObject("search-results");
            }
            catch(Exception ex) {
                System.out.println("Scopus API fail");
            }
        }
        
        Integer total = Integer.parseInt(temp.getString("opensearch:totalResults"));
        Integer loops = total / 25 + 1;
        Integer index = 0;
        Logger.write(" - Total: " + total);
        
        for(int counter=0; counter<loops; counter++) {
            query = "/search/scopus/?query=AU-ID(" + scopusid.replace("9-s2.0-", "") + ")&apiKey=" + api_key + "&start=" + 25*counter;
            
            temp = null;
            while(temp == null) {
                result = http.sendGet(url + query);
                
                for(int j=0; j<10;j++)
                    result = result.replaceFirst("link", "L_new_ink" + j);
            
                for(int j=0; j<10;j++)
                    result = result.replaceFirst("subtype", "sub_new_type" + j);
        
                try {
                    temp = new JSONObject(result).getJSONObject("search-results");
                }
                catch(Exception e) {
                    System.out.println("Scopus API fail");
                }
            }     
            
            JSONArray ids = null;
            try { ids = temp.getJSONArray("entry"); } catch(Exception e) { Logger.write("Warning, non papers founs"); };
            
            if(ids != null) {
                for(int i=0; i<ids.length(); i++) {
                    JSONObject paperTemp = ids.getJSONObject(i);

                    String id = "unknown";
                    String title = "unknown";
                    String year = "unknown";
                    String type = "unknown";
                    String citations = "0";

                    try { id = paperTemp.getString("dc:identifier").replace("SCOPUS_ID:", ""); } catch (Exception e) { Logger.write("Warning, paper identifier not found"); };
                    try { title = paperTemp.getString("dc:title"); } catch (Exception e) { Logger.write("Warning, paper title not found"); }
                    try { year = paperTemp.getString("prism:coverDisplayDate"); } catch (Exception e) { Logger.write("Warning, paper year not found"); }
                    try { type = paperTemp.getString("prism:aggregationType"); } catch (Exception e) { Logger.write("Warning, paper type not found"); }
                    try { citations = paperTemp.getString("citedby-count"); } catch (Exception e) { Logger.write("Warning, paper citations not found"); }

                    Paper paper = new Paper(id, title, year, type, citations);
                    papers.add(paper);

                    Logger.write("  - [" + index + "] " + paper.toString());
                    index++;
                }
            }
        }
        
        Logger.write("");
        return papers;
    }
}
