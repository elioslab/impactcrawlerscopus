package elios.impactcrawler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.codehaus.jackson.map.ObjectMapper;

public class Utils {
    public static Date getDate() {
        return new Date();
    }

    public static void toJSONFile(Aggregate aggregate) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File("output/aggregate.json");
        file.delete();
        mapper.writeValue(file, aggregate);
    }
    
    public static void toChart(Aggregate aggregate) throws Exception
    {
        ExportChart chart = new ExportChart();
        
        for(int i=0; i<aggregate.getFrequencies().size(); i++) {
            String name = aggregate.getFrequencies().get(i).getName();
            
            Map<String, Integer> citations = aggregate.getFrequencies().get(i).getCitations();
            Map<String, Integer> hindexes = aggregate.getFrequencies().get(i).getHindexes();
            Map<String, Integer> documents = aggregate.getFrequencies().get(i).getDocumens();
            Map<String, Integer> journals = aggregate.getFrequencies().get(i).getJournalDocuments();
            Map<String, Integer> conferences = aggregate.getFrequencies().get(i).getConferenceDocuments(); 
           
            chart.generate(citations, "Frequency - " + name + " - Citation", "Number of citations", "Members");
            chart.generate(hindexes, "Frequency - " + name + " - H-index", "H-Index", "Members");
            chart.generate(documents, "Frequency - " + name + " - Papers", "Number of papers","Members");
            chart.generate(journals, "Frequency - " + name + " - Journal Papers", "Number of journal papers", "Members");
            chart.generate(conferences, "Frequency - " + name + " - Conference Papers", "Number of conference papers", "Members");
        }

        for(int i=0; i<aggregate.getPercentiles().size(); i++) {
            String name = aggregate.getPercentiles().get(i).getName();

            Map<String, Integer> citations_percentile = aggregate.getPercentiles().get(i).getCitations();
            Map<String, Integer> hindexes_percentile = aggregate.getPercentiles().get(i).getHindexes();
            Map<String, Integer> documents_percentile = aggregate.getPercentiles().get(i).getDocumens();
            Map<String, Integer> journals_percentile = aggregate.getPercentiles().get(i).getJournalDocuments();
            Map<String, Integer> conferences_percentile = aggregate.getPercentiles().get(i).getConferenceDocuments();

            chart.generate(citations_percentile, "Percentile - " + name + " - Citation", "Number of citations", "% of members");
            chart.generate(hindexes_percentile, "Percentile - " + name + " - H-index", "H-Index", "% of members");
            chart.generate(documents_percentile, "Percentile - " + name + " - Papers", "Number of papers","% of members");
            chart.generate(journals_percentile, "Percentile - " + name + " - Journal Papers", "Number of journal papers", "% of members");
            chart.generate(conferences_percentile, "Percentile - " + name + " - Conference Papers", "Number of conference papers", "% of members");
        }
    }
    
    public static void toCSVFile(Aggregate aggregate) throws IOException
    {
        toCSVFile_filter(aggregate, null);
        toCSVFile_filter(aggregate, "Ricercatore");
        toCSVFile_filter(aggregate, "Associato");
        toCSVFile_filter(aggregate, "Ordinario");
        toCSVFile_frequency(aggregate.getFrequencies());
        toCSVFile_percentiles(aggregate.getPercentiles());
        toCSVFile_medians("output/medians.csv", aggregate.getMedians());
    }
    
    public static void toCSVFile_percentiles(List<Distribution> percentiles) {
        List<String> names = new ArrayList<>();
        List<Map<String, Integer>> hindexes = new ArrayList<>();
        List<Map<String, Integer>> documents = new ArrayList<>();
        List<Map<String, Integer>> journals = new ArrayList<>();
        List<Map<String, Integer>> conferences = new ArrayList<>();
        List<Map<String, Integer>> citations = new ArrayList<>();

        for(int i=0; i<percentiles.size(); i++) {
            names.add(percentiles.get(i).getName());
            hindexes.add(percentiles.get(i).getHindexes());
            documents.add(percentiles.get(i).getDocumens());
            journals.add(percentiles.get(i).getJournalDocuments());
            conferences.add(percentiles.get(i).getConferenceDocuments());
            citations.add(percentiles.get(i).getCitations());
        }

        Utils.toCSVFile_map("output/percentiles/percentile_hindex", names, hindexes);
        Utils.toCSVFile_map("output/percentiles/percentile_papers", names, documents);
        Utils.toCSVFile_map("output/percentiles/percentile_journals", names, journals);
        Utils.toCSVFile_map("output/percentiles/percentile_conferences", names, conferences);
        Utils.toCSVFile_map("output/percentiles/percentile_citations", names, citations);    
    }
    
    public static void toCSVFile_frequency(List<Distribution> frequencies) throws IOException {
        List<String> names = new ArrayList<>();
        List<Map<String, Integer>> hindexes = new ArrayList<>();
        List<Map<String, Integer>> documents = new ArrayList<>();
        List<Map<String, Integer>> journals = new ArrayList<>();
        List<Map<String, Integer>> conferences = new ArrayList<>();
        List<Map<String, Integer>> citations = new ArrayList<>();
        
        for(int i=0; i<frequencies.size(); i++)
        {
            names.add(frequencies.get(i).getName());
            hindexes.add(frequencies.get(i).getHindexes());
            documents.add(frequencies.get(i).getDocumens());
            journals.add(frequencies.get(i).getJournalDocuments());
            conferences.add(frequencies.get(i).getConferenceDocuments());
            citations.add(frequencies.get(i).getCitations());
        }
        
        toCSVFile_map("output/frequencies/frequency_hindex", names, hindexes);
        toCSVFile_map("output/frequencies/frequency_papers", names, documents);
        toCSVFile_map("output/frequencies/frequency_journals", names, journals);
        toCSVFile_map("output/frequencies/frequency_conferences", names, conferences);
        toCSVFile_map("output/frequencies/frequency_citations", names, citations);
    }
    
    public static void toCSVFile_map(String title, List<String> names, List<Map<String, Integer>> values) {
        File file = new File(title + ".csv");
        file.delete();
        
        BufferedWriter writer = null;
        
        try {
            writer = new BufferedWriter(new FileWriter(file, true));
            
            for(int i=0; i<values.size(); i++) {
                String name = names.get(i);
                
                String line = name + ";";
            
                Object[] value_names = values.get(i).keySet().toArray();
                for(int j=0; j<value_names.length; j++)
                    line += value_names[j] + ";"; 
            
                writer.append(line);
                writer.newLine();
                
                Object[] value_ints = values.get(i).values().toArray();
                
                line = "values;";
                
                for(int j=0; j<value_ints.length; j++)
                    line += value_ints[j] + ";";
                
                writer.append(line);
                writer.newLine();
                
                writer.newLine();
            }
            
            writer.close();              
        }
        catch (IOException ex)  {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                writer.close();
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void toCSVFile_medians(String name, List<Metric> medians) throws IOException {
        File file = new File(name);
        file.delete();
        
        BufferedWriter writer = null;
        
        try {
            writer = new BufferedWriter(new FileWriter(file, true));
            
            String line = "";

            line += "Name" + ";";
            line += "H-index" + ";";
            line += "Citations" + ";";
            line += "All Papers" + ";";
            line += "Journal Papers" + ";";
            line += "Conference Papers" + ";";
            line += "Start Year" + ";";            
            line += "Stop Year" + ";";
            
            writer.append(line);
            writer.newLine();
            
            for(int i=0; i<medians.size(); i++) {
                Metric median = medians.get(i);

                line = "";

                line += median.getName() + ";";
                line += median.getHindex() + ";";
                line += median.getCitationCount() + ";";
                line += median.getDocumentCount() + ";";
                line += median.getJournalDocumentCount() + ";";
                line += median.getConferenceDocumentCount() + ";";
                line += median.getStartYear() + ";";
                line += median.getStopYear() + ";";
                
                writer.append(line);
                writer.newLine();
            }
            
            writer.close();              
        }
        catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                writer.close();
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            }
        }       
    }
    
    private static void toCSVFile_filter(Aggregate aggregate, String filter) throws IOException {
        File file = null;
        
        if(filter == null) 
            file = new File("output/aggregate_all.csv");
        else
            file = new File("output/aggregate_" + filter + ".csv");
        
        file.delete();
        
        BufferedWriter writer = null;
        
        try {
            writer = new BufferedWriter(new FileWriter(file, true));
            
            List<Member> members = aggregate.getMembers();
            
            String line = "";

            line += "ScopusID" + ";";
            line += "Name" + ";";
            line += "Surname" + ";";
            line += "Role" + ";";
            line += "SSD" + ";";
            
            List<Metric> metrics = members.get(0).getMetrics();
            for(int i=0; i<metrics.size(); i++) {
                String start = metrics.get(i).getStartYear().toString();
                String stop = metrics.get(i).getStopYear().toString();

                line += "Total Papers (" + start + " - " + stop + ")" + ";";
                line += "Journal Papers (" + start + " - " + stop + ")" + ";";
                line += "Conference Papers (" + start + " - " + stop + ")" + ";";
                line += "Citations (absolute) (" + start + " - " + stop + ")" + ";";
                line += "H-Index (absolute) (" + start + " - " + stop + ")" + ";";
            }
            
            writer.append(line);
            writer.newLine();
            
            for(int i=0; i<members.size(); i++)
            {
                Member member = members.get(i);
                
                if( (filter == null) || (member.getRole().contains(filter)) )
                {
                    line = "";

                    line += member.getScopusid() + ";";
                    line += member.getName() + ";";
                    line += member.getSurname() + ";";
                    line += member.getRole() + ";";
                    line += member.getSSD();

                    metrics = member.getMetrics();
                    for(int j=0; j<metrics.size(); j++)
                    {
                        Metric metric = metrics.get(j);

                        line += metric.getDocumentCount() + ";";
                        line += metric.getJournalDocumentCount() + ";";
                        line += metric.getConferenceDocumentCount() + ";";
                        line += metric.getCitationCount() + ";";
                        line += metric.getHindex() + ";";
                    }

                    writer.append(line);
                    writer.newLine();
                }
            }
            
            writer.close();
        }
        catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                writer.close();
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            }
        }       
    }
}
